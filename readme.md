﻿# RavenDB - the premier NoSQL database for .NET

This repository contains source code for [RavenDB](http://ravendb.net/) document database.

Build Status
------------
| Version | Status |
|:-------:|:-------|
| 3.0 | ![](http://teamcity.hibernatingrhinos.com/app/rest/builds/buildType:(id:RavenDBTests_30Tests)/statusIcon) |
| 3.5 | ![](http://teamcity.hibernatingrhinos.com/app/rest/builds/buildType:(id:RavenDBTests_35Tests)/statusIcon) |

New to RavenDB?
---------------
Check out our [Getting started page](http://ravendb.net/docs/article-page/3.5/csharp/start/getting-started).

How to download?
-----------------------
| Stable | [download](http://ravendb.net/downloads) | [NuGet](https://www.nuget.org/packages/RavenDB.Server) |
|:-------:|:-------:|:-------:|
| Unstable | [download](http://ravendb.net/downloads/builds) | [NuGet](https://www.nuget.org/packages/RavenDB.Server) |
| .NET Client | [download](http://ravendb.net/downloads) | [NuGet](https://www.nuget.org/packages/RavenDB.Client) |
| Java Client | [download](http://ravendb.net/downloads) | [Maven](http://search.maven.org/#search%7Cgav%7C1%7Cg%3A%22net.ravendb%22%20AND%20a%3A%22ravendb-client%22) |

What's new?
-----------
Our latest list of changes can always be found [here](http://ravendb.net/docs/article-page/3.5/csharp/start/whats-new).

Found a bug?
------------
You can create issues at our [YouTrack](http://issues.hibernatingrhinos.com).

Need help?
----------
If you have any questions please visit our [community group](http://groups.google.com/group/ravendb/).